<INC-CONTENTS>
   <CONTENT for="/united-kingdom/" label="Welcome to The XContest UK!">
      <div class="wsw">
<h2 class="faq-c">Welcome to The XContest UK!</h2>
<p>This is a <i>three flight</i> XC league for paraglider and hang glider pilots in the UK. We want to promote the adventure of XC flying on our small islands and encourage you on, whatever you fly!</p> 

<h3>What’s the big idea?</h3>
<p>A lot of thought has gone into this new format. Firstly, a three flight score helps make things more equal for those in areas where opportunities to score big days are rarer. Secondly, we’re leveling the playing field with a handicap system – meaning that EN A, B and C paraglider pilots can score more equally each day with higher performance flyers. Third, the multipliers for triangles and ‘free’ triangles encourage circuit flying on lightwind days, but equalise the challenges of such flights with the complexities of open distance flying. We think scoring should reflect the fact that there’s only been two 300 km straight line flights in the UK done by paraglider (Richard Carter and Hugh Miller), and two by hang glider (Carl Wallbank and Nev Almond).</p>


<p>Also, we’re keen to award prizes for the big milestones, to help celebrate your achievements. Fly your first 100 km straight line XC and you’ll be sent a Cross Country XContest 100 T-shirt. Go on to crack your first 200 points, and you’ll earn a Cross Country Speed Top (with gold sleeves).</p>

<p>Above all this is a pilot-represented league, run by a committee, who are here to listen to pilots’ views and shape the contest accordingly.</p>
 
<p>The XContest UK ends 30 September, and restarts on 1 October each year.</p>
      </div>
   </CONTENT>

   <CONTENT for="/united-kingdom/rules/">
      <div class="wsw">
         <h3 class="faq-q">The XContest UK is...</h3>

<strong>1. Simple:</strong> There’s no need to declare tasks (and risk invalidation due to an instrument error) - just plan and fly the day as best you can. The system will score you automatically as either open distance, or closed circuit.<br />
<strong>2. Automated:</strong> Download the free XCtrack app (or use your preferred live-tracking platform, such as FlySkyHy) to automatically log your flights when you land. <br />
<strong>3. Open to all:</strong> It is open to hang glider and paraglider pilots<br />
<strong>4. Fair ranking:</strong> There’s an open category featuring all classes, with handicaps to even things out, and each category of glider has its own individual class ranking, too.<br />
<strong>5. Your chance to win a cup: </strong> A small one, but a meaningful one.<br />

<h3 class="faq-q">Rules</h3>

<b>1. Be quick:</b> Submit your flight tracklog (IGC file) within 14 days. Our system is robust and accepts a wide range of file formats. For 2023, we will accept back-dated flights to 1 April 2023.<br/>
<b>2. Be qualified:</b> You must be a  BHPA member with an appropriate qualification and rating for the flying you are doing.<br/> 
<b>3. Be good with airspace:</b> Flights must be conducted in accordance with UK law and the CAA Skyways Code. The pilot is solely responsible for ensuring this compliance. If a flight infringes airspace, it is disqualified. There is zero tolerance, and zero tolerances. <br/>
<b>4. Be prepared to donate!</b> If you submit a flight that infringes permanent or temporary airspace (eg a NOTAM), you will be asked to make a £50 donation to your local Air Ambulance charity, and your flight will be removed.<br/>
<b>5. Be helpful:</b> The XContest UK needs to be self-marshaling. If you spot a problem with someone else’s flight, contact them and work it out together! Please use your instrument manufacturer’s support to help you with any technical issues.<br/>
<b>6. And finally:</b> The rules and multipliers are based around current national records, average time to fly and overall altitude gain required for each flight type, based on existing flight data.<br/>

<h3 class="faq-q">Scoring</h3>

<b>Free distance:</b> 1 km = 1 point<br />
<b>Closed Free Triangle:</b> 1 km = 1.8 points (a closed circuit flight using three points, with a maximum circuit gap of 800 m between start and finish. No minimum shortest leg)<br />
<b>Closed FAI Triangle:</b> 1 km = 2.1 points  (a closed circuit flight with a maximum circuit gap of 800 m between start and finish. The shortest leg of the triangle must be at least 28% of the total triangle distance)<br />

<b>Categories:</b> Hang-glider (FAI 1), Rigid (FAI 5), Paraglider: Open (CCC) Serial (EN D) Sport (EN C) Standard (EN A and EN B) and Tandem<br/>
<b>Handicaps:</b>  Within the Open and club rankings, standard (EN A and EN B) category and tandem paraglider pilots will benefit from a 1.25 multiplier. Sport (EN C) paragliders score at a 1.1 multiplier. Within the club rankings, those flying rigid hang gliders will score at 0.8, and flexwing hang gliders at 0.7. They’re not perfect, but they’ll help!<br/>

<b>Compete for your club or team:</b> The top 4 pilots from each club score. The XContest UK also includes ranking for North and South pilots for the annual North South Cup.<br/>


<p>We encourage UK pilots to also log flights in the official UK XC League (www.xcleague.com) – not least because it helps the BHPA gather evidence of our airspace needs.</p>

<h3 class="faq-q">How To Enter</h3>
<p>To enter, just REGISTER for this competition and all flights you submit to XContest after the date you register will show up here.</p>

<p>Any flights you entered into the World XContest BEFORE registration into the UK XContest will not be claimed automatically (the process is automatic only if the flight is claimed AFTER your registration). Please go to World XContest - My flights -> edit flight -> add checkmark to XContest UK -> Save.</p>

<p>For 2023, entry is free. This competition is kindly supported by XContest and Cross Country (www.xcmag.com) along with independent prize sponsors to be announced.</p>

<h3 class="faq-q">How to Claim a Prize</h3>
<p>To claim a prize, email your name, address, T-shirt size and a link to your flight to <a href="mailto:verity@xcmag.com">verity@xcmag.com</a>. Your flight has to be a <b>first</b> for you in the UK (here or in any other league), done on trust. Remember, your 100 km claim needs to be a straight line distance, not via turnpoints. The 200 points prize is for your first flight netting 200 points (either a circuit, or free distance via 3 turnpoints). Please <b>do not</b> contact verity@xcmag.com for any technical or other issues - prize claims only. Thanks.</p>

<p>Three is the magic number...go fill your boots!</p>
      </div>
   </CONTENT>

   <CONTENT for="/2022/united-kingdom/" label="Welcome to the XCONTEST UK">
      <div class="wsw">
	 <p>
	 <a target="_blank" href="https://flywithgreg.com/?utm_source=xcuk&amp;utm_medium=banner&amp;utm_campaign=202103&amp;utm_content=welcome"><img src="https://drive.google.com/uc?id=17TXlietVaTqtnIDt_o2xAE7OwVMytBgD" width="100%" title="XContest UK" alt="XContest UK cover photo" /></a><br /><br />
<h2 class="faq-c">Welcome to XContest UK</h2>
Share your XC adventures and learn from the tracklogs of others. XContest UK is open to all pilots and there is a simple ranking system: your top 6 flights that started in the United Kingdom count. This seasonal competition begins on 1 October every year.

<h3 class="faq-q">This is different!</h3>

<strong>1. Simple:</strong> No need to declare tasks, just go fly and the system will score you as either open distance, or closed circuit.<br />
<strong>2. Automated:</strong> No computer hassles! Download the free XCtrack app (or use your preferred livetracking platform) to automatically log your flights when you land. <br />
<strong>3. Unifying freeflight:</strong> It is open to hang-gliders and paragliders<br />
<strong>4. Fair ranking:</strong> Each category of aircraft has its own ranking, and paragliders have various classes.<br />
<strong>5. Compete for your club!</strong> The best 6 pilots from each club help to elevate its reputation.<br />

<h3 class="faq-q">Rules</h3>
<b>1. Be quick:</b> Submit your flight tracklog (IGC file) within 14 days. Our system is robust and accepts a wide range of file formats.<br/>
<b>2. Be good:</b> Flights must be conducted in accordance with UK law. The pilot is solely responsible for ensuring this compliance. If your flight passes through airspace, the flight is disqualified unless you email evidence of your permission, to <b>xc at flywithgreg.com</b>.<br/>
<b>3. Be helpful:</b> This platform just collects flight tracks to help pilots have access to a simple inclusive learning tool and fun competition. We don't process complaints, police the sport or listen to politics. Please use Google and Youtube to solve your technical issues.

<h3 class="faq-q">Scoring</h3>
<b>Free distance:</b> 1 km = 1 point<br />
<b>Flat triangle:</b> 1 km = 1.5 points (a closed circuit flight with less than 5% of the total distance remaining to return to the start)<br />
<b>FAI Triangle:</b> 1 km = 2 points (the shortest leg of the triangle must be at least 28% of the total triangle)<br />
<b>Club scores:</b> Hang-gliders are handicapped by 0.8 and Rigid-Wings by 0.7 in the club scoring<br />
<b>Categories:</b> Hang-glider (FAI 1), Rigid (FAI 5), Paraglider: Open (CCC) Serial (EN D) Sport (EN C) Standard (EN A/B) and Tandem

<h3 class="faq-q">Entry Fee</h3>
<b>It is free!</b> This competition is financed by Greg Hamerton as a pilot development project. To improve your XC skills or show your support you can become a member on <a href="https://flywithgreg.com/">FlyWithGreg.com</a>. 
If you like XContest UK, tell your friends about it, and go fly XC. <br />
<br />
To enter, just REGISTER for this competition and all flights you submit to XContest AFTER THAT DATE will show up here.

<h3 class="faq-q">Having Trouble?</h3>
Flights you claimed BEFORE registration into the UK contest will not be claimed automatically (the process is automatic only if the flight is claimed AFTER your registration). Please go to World XContest - My flights -> edit flight -> add checkmark to XContest UK -> Save. <br />
<br />
If you have questions or comments please contact the administrator via xc at flywithgreg.com<br />
</p>
      </div>
   </CONTENT>

</INC-CONTENTS>
